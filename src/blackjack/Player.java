package blackjack;
import java.util.*;

/**
 * Created by Arnav on 11/16/14.
 */
public class Player {

    private int handScore;
    private int money;
    private ArrayList<Card> hand;

    public Player() {
        handScore = 0;
        money = 1000;
        hand = new ArrayList<Card>();
    }

    public int getMoney() {
        return money;
    }

    public int getHandScore() {
        return handScore;
    }

    public void resetHand() {
        handScore = 0;
    }

    public void updateMoney(int amount) {
        money += amount;
    }

    public void addCard(Card card) {
        hand.add(card);
        handScore += card.getValue();
    }


}
