package blackjack;
import java.util.*;
import java.io.*;

/**
 * Created by Arnav on 11/16/14.
 */
public class Deck {

    Map<Integer,Card> deck;

    public Deck() {
        deck = null;

        try {
            deck = createDeck();
        } catch (FileNotFoundException e) {
            System.out.println("file containing card images not found " + e);
        }

    }

    // create hashmap of cards;
    private HashMap<Integer, Card> createDeck() throws FileNotFoundException {
        HashMap<Integer, Card> createdDeck = null;
        File file = new File("card_images/cardnames.txt");
        Scanner in = new Scanner(file);
        int i = 0;
        while(in.hasNextLine()) {
            String fName = in.nextLine();
            int cardValue = getCardValue(fName);
            try {
                createdDeck.put(i,new Card(cardValue,fName));
            } catch(IllegalArgumentException e) {
                System.out.println(" bad value for card " + e);
            }
            i++;
        }
        return createdDeck;
    }

    // private helper method to get card value from card file name
    private int getCardValue(String fName) {
        if (fName.substring(0,fName.indexOf('_')).equalsIgnoreCase("ace"))
            return 11;
        else if (fName.substring(0,fName.indexOf('_')).equalsIgnoreCase("king"))
            return 10;
        else if (fName.substring(0,fName.indexOf('_')).equalsIgnoreCase("queen"))
            return 10;
        else if (fName.substring(0,fName.indexOf('_')).equalsIgnoreCase("jack"))
            return 10;
        else {
            return Integer.parseInt(fName.substring(0,fName.indexOf('_')));
        }
    }

    //simulate drawing card from 6 deck shoe
    public Card draw() {
        int cardIndex = (int) ((Math.random()*312)/6.0);
        return deck.get(cardIndex);
    }
}
