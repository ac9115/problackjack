package blackjack;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Arnav on 11/16/14.
 */
public class BlackJackGUI extends JFrame {
    private BlackJackGame game;
    private JPanel panel1;
    private JPanel CPUPanel;
    private JPanel PlayerPanel;
    private JButton Hit;
    private JButton Stay;
    private JTextArea CPUScore;
    private JTextArea UserScore;
    private JLabel CPUCard1;
    private JLabel CPUCard2;
    private JLabel PlayerCard1;
    private JLabel PlayerCard2;
    private JButton StartButton;
    private JPanel StartArea;
    private JPanel UserInteractionArea;
    private JTextArea MoneyLeft;

    public BlackJackGUI() {
        createUIComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(500,500);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("MY UI");
        frame.setContentPane(new BlackJackGUI().panel1);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        PlayerCard1 = new JLabel();
        PlayerCard2 = new JLabel();
        game = new BlackJackGame();

        StartButton = new JButton("Start");
        StartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game = new BlackJackGame();
                PlayerCard1 = new JLabel();
                Card card1 = game.deal();
                Card card2 = game.deal();
                PlayerCard1.setIcon(new ImageIcon(card1.getCardImage()));
                PlayerCard2.setIcon(new ImageIcon(card2.getCardImage()));
            }
        });

        Hit = new JButton("Hit");
        Hit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Card dealtCard = game.deal();
                PlayerCard1.setIcon(new ImageIcon(dealtCard.getCardImage()));
            }
        });

        Stay = new JButton("Stay");
        Stay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"To be done");
            }
        });

        UserScore = new JTextArea();
        CPUScore = new JTextArea();

        StartArea = new JPanel();
        StartArea.add(StartButton);
        StartArea.add(MoneyLeft);

        UserInteractionArea = new JPanel();
        UserInteractionArea.add(Hit);
        UserInteractionArea.add(Stay);
        UserInteractionArea.add(UserScore);
        UserInteractionArea.add(CPUScore);


    }
}
