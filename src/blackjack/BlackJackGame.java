package blackjack;

/**
 * Created by Arnav on 11/16/14.
 */
public class BlackJackGame {
    
    private Deck deck;
    private Player player;
    private int betAmount;
    private Card lastCardDrawn;
    
    public BlackJackGame() {
        try {
            deck = new Deck();
        } catch (NullPointerException e) {
            System.out.println("Deck not created properly " + e);
        }
        
        player = new Player();
        deal();
        betAmount = 10;
    }

    public int getBetAmount() {
        return betAmount;
    }

    public Card getLastCardDrawn() {
        return lastCardDrawn;
    }

    public Card deal() {
        lastCardDrawn = deck.draw();
        player.addCard(lastCardDrawn);
        return lastCardDrawn;
    }

    public void hit() {
        player.addCard(deck.draw());
    }

    public boolean isBust() {
        return player.getHandScore() > 21;
    }

    public boolean isBlackJack() {
        return player.getHandScore() == 21;
    }
}
