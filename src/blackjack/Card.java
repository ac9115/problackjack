package blackjack;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

/**
 * Created by Arnav on 11/16/14.
 */
public class Card {

    private int value;
    private BufferedImage cardImage;

    public Card(int value, String imageFile) {
        if (value < 1 || value > 11)
            throw new IllegalArgumentException("Illegal Value for card");
        this.value = value;
        cardImage = null;
        try {
            cardImage = ImageIO.read(new File(imageFile));
        } catch (IOException e) {
            System.out.println("Oh no! something went wrong with the file name" + e);
        }
    }

    public int getValue() {
        return value;
    }

    public BufferedImage getCardImage() {
        return cardImage;
    }
}
