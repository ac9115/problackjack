package Yoda;
import java.util.*;

/**
 * Created by Arnav on 11/16/14.
 */
public class YodaSpeak {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String line = in.nextLine();
        System.out.println("Yoda says: " + yodaSpeak(line));
    }

    private static String yodaSpeak(String line) {
        String[] lineSplit = line.split(" ");
        String yodaed = "";

        for (int i = lineSplit.length - 1; i > -1; i--)
            yodaed += lineSplit[i] + " ";

        return yodaed;
    }
}
