package Yoda;
import java.util.*;

/**
 * Created by Arnav on 11/16/14.
 */
public class YodaSpeakRecursive {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String line = in.nextLine();
        System.out.println("Yoda says: " + yodaSpeakRecursive(line));
    }

    private static String yodaSpeakRecursive(String line) {
        if (line.lastIndexOf(' ') == -1)
            return line;
        else {
            return line.substring(line.lastIndexOf(' ') + 1) + " " +
                    yodaSpeakRecursive(line.substring(0,line.lastIndexOf(' ')));
        }
    }

}
